coins: int = [
    1, 3, 7, 10, 11, 14, 21, 25, 30, 40
]#Coins Value

bank: dict = {
    1: 0, 3: 0, 7: 0, 10: 0, 11: 0, 14: 0, 21: 0, 25: 0, 30: 0, 40: 0
}#Store and counts the coins

def print_bank(which_bank: dict):#Print bank but only have values.
    for i in coins:
        if which_bank.get(i) != 0:
            print(i, ": ", which_bank[i])

def scan(which_bank: dict) -> dict:#Same as bank but it will only take the keys and return with values.
    temp_bank = {}
    for i in coins:
        if which_bank.get(i) != 0:
            temp_bank[i] = which_bank.get(i)
    return temp_bank

def greedy_check(input : int) -> dict:#Check and show from greatest to least solution coins. 
    temp_bank: dict = bank.fromkeys(bank, 0)
    value = input
    for i in reversed(coins):
        while value >= i:#Keep running until i < value
            temp_bank[i] += 1
            value -= i
    return temp_bank


def checker(input: int, id: int, banc: dict) -> dict:#Bruteforce check
    temp_bankk = banc
    value = input
    while value > 0:
        if value >= coins[id]:
            temp_bankk[coins[id]] += 1
            value -= coins[id]
            temp_bankk["value"] = value
            temp_bankk["n"] += 1
        else:
            temp_bankk = checker(value, id - 1, temp_bankk)
            value = temp_bankk["value"]

    return temp_bankk

def optimal_check(input: int) -> None:#While in bruteforce it finding the best solutions from greatest to least
    value = input
    solutions = 0
    best = {}
    n = 0
    for i in range(len(coins)):
        if value < coins[i]: break

        print("\nSolution #", solutions + 1)
        temp_bank: dict = bank.fromkeys(bank, 0)
        temp_bank["value"] = input
        temp_bank["best"] = best #Save the best solution
        temp_bank["n"] = 0
        print("----------")
        print_bank(checker(input, i, temp_bank))

        if n == 0:
            temp_bank["best"] = scan(temp_bank)
            best = temp_bank["best"]
            n = temp_bank["n"]
        else:
            if n >= temp_bank["n"]:#Previous >= Current
                temp_bank["best"] = scan(temp_bank)
                best = temp_bank["best"]
                n = temp_bank["n"]

        solutions += 1
        print("- - - - - -")
        print(temp_bank["n"], " coin(s)")

    print("---------------")
    print("Solutions: ", solutions)
    print("Best[", n, " coin(s)]: ", best)
    return None

while True:
    answer =  input("Enter a number(X to exit): ")
    if answer == "X" or answer == "x": break
    else:
        try:
            answer = int(answer)#You can only input a integer
            print("Greedy Check:")#It will print the Greedy Check function
            print_bank(greedy_check(answer))
            print("Bruteforce Check:")#It will print the Bruteforce check
            optimal_check(answer)#It will print the input coin with the best solution in it.
        except ValueError:
            print("Integers or [X] only.")
