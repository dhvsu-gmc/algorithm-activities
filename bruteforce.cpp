#include <iostream>
#include <string>

using namespace std;

bool lowercase(char subject){//check if the character is a lowercase
    /*even more ineffiient bruteforcing
    char lowercase_list[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    for(char letter: lowercase_list){
        if(subject == letter){
            return true;
        }
    }*/
    if(subject >= 'a' && subject <= 'z'){//if it fits the range a-z, it's a lowercase
        return true;
    }
    return false;
}

bool uppercase(char subject){//check if the character is an uppercase
    /*even more ineffiient bruteforcing
    char uppercase_list[26] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    for(char letter: uppercase_list){
        if(subject == letter){
            return true;
        }
    }*/
    if(subject >= 'A' && subject <= 'Z'){//if it fits the range A-Z, it's an uppercase
        return true;
    }
    return false;
}

bool integer(char subject){//check if the character is an integer
    /*even more ineffiient bruteforcing
    char number_list[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    for(char number: number_list){
        if(subject == number){
            return true;
        }
    }*/
    if(subject >= '0' && subject <= '9'){//if it fits the range 0-9, it's an integer
        return true;
    }
    return false;
}
string real_number(string list, int start_pos, bool dot=false){//Algorithm for checking real numbers
    //tried making an iterative algorithm but proved to be more complex to write than a recursion
   string real_numbers= "";//store everything here
   if(integer(list[start_pos])){
       real_numbers += list[start_pos];//include any integers that followed
       real_numbers = real_numbers.append(real_number(list, start_pos + 1, dot));//continue checking the list
   }
   else if(list[start_pos] == '.' && !dot){
       real_numbers += list[start_pos];//include decimal point in real numbers
       real_numbers = real_numbers.append(real_number(list, start_pos + 1, true));//next
   }
   else if(list[start_pos] == '.' && dot){//do not count double dots
       real_numbers = "";

       return real_numbers;
   }

   /*if(!dot){
       real_numbers = "";
       real_numbers += list[start_pos];
   }*/
   return real_numbers;
}

bool parenthesis(char subject){//check if the character is either a ( or a )
    if(subject == '(' || subject == ')'){
        return true;
    }
    return false;
}

bool pluss(char subject){//check if the character is a +
    if(subject == '+'){
        return true;
    }
    return false;
}

bool minuss(char subject){//check if the character is a -
    if(subject == '-'){
        return true;
    }
    return false;
}

bool divide(char subject){//check if the character is a /
    if(subject == '/'){
        return true;
    }
    return false;
}

bool multiply(char subject){//check if the character is a *
    if(subject == '*'){
        return true;
    }
    return false;
}

bool moduluss(char subject){//check if the character is a %
    if(subject == '%'){
        return true;
    }
    return false;
}

void check(string input){//main guts of the system, had to branch it out of the main function since it was growing out of hand
    string log[input.length()], //log the type of the elements 
    INTEGER = " is an integer.", //made them variables for convenience
    LOWERCASE = " is a lowercase letter.",
    UPPERCASE = " is an uppercase letter.",
    SPECIAL = " is a special character.",
    REAL = " is a real number.",
    PARENTHESIS = " is a parenthesis.",
    PLUS = " is a plus sign.",
    MINUS = " is a minus sign.",
    DIVIDE = " is a division sign.",
    MULTIPLY = " is a multiplication sign.",
    MODULUS = " is a modulus sign.";
    for (size_t i = 0; i < input.length(); ++i){//check the type of each character in the input string
            if(lowercase(input[i])){
                log[i] = LOWERCASE;
                cout << input[i] << log[i] << endl;
            }
            else if(uppercase(input[i])){
                log[i] = UPPERCASE;
                cout << input[i] << log[i] << endl;
            }
            else if(parenthesis(input[i])){
                log[i] = PARENTHESIS;
                cout << input[i] << log[i] << endl;
            }
            else if(pluss(input[i])){
                log[i] = PLUS;
                cout << input[i] << log[i] << endl;
            }
            else if(minuss(input[i])){
                log[i] = MINUS;
                cout << input[i] << log[i] << endl;
            }
            else if(divide(input[i])){
                log[i] = DIVIDE;
                cout << input[i] << log[i] << endl;
            }
            else if(multiply(input[i])){
                log[i] = MULTIPLY;
                cout << input[i] << log[i] << endl;
            }
            else if(moduluss(input[i])){
                log[i] = MODULUS;
                cout << input[i] << log[i] << endl;
            }
            else if(integer(input[i])){//integers may form real numbers if they meet a specific requirement
                if(i ==  input.length() - 1){//If the integer is the last element, just print it
                    log[i] = INTEGER;
                    cout << input[i] << log[i] << endl;
                }
                else{//Check for the next element
                    string number = real_number(input, i);
                    if(number.length() > 1){//this meant the result was a real number, since the real number algorithm used will only spit out a string of length > 1
                        log[i] = REAL;
                        cout << number << log[i] << endl;
                        i += number.length() - 1;//skip to the next non-real number character
                    }
                    else{//means it failed to become a real number, it was a spy all along
                        log[i] = INTEGER;
                        cout << input[i] << log[i] << endl;
                    }
                }
            }
            else{//other symbols are special
                log[i] = SPECIAL;
                cout << input[i] << log[i] << endl;
            }
            
        }
    cout << endl; //Maintain distance
}

int main(){//where the program begins
    check("(a+B)/2*1.2%@\\");
    check("(&4+(32.55jJ3*");
    check("Dn4/)3.14he-");
    
    
    return 0;//where the program ends
}