hash_table: dict = {}#dictionary hash table
max_size: int = 100#MAX SIZE OF TABLE

for i in range(max_size):#populate the table
    hash_table[str(i)] = ""

def add_hash(value: str):#Put Value
    if len(value) > 4 or len(value) < 2:#Check if the value's length is greater than 4 or less than 2
        print("Accepts 2-4 characters only.")
        return#If the value's length does not meet the condition if wil not accept it
    for i in range(max_size):#Iterate the hash table
        if hash_table[str(i)] == hash(value):#check if the key is already existing
            print("Hash " + str(hash_table[str(i)]) + "  already exists in key [" + str(i) + "].")#Output to show the key is added
            return
    for i in range(max_size):#Iterate the hash table
        if hash_table[str(i)] == "":#Check if the key is empty
            hash_table[str(i)] = hash(value)#If empty it will assign the hash value in the key
            print("Hash " + str(hash_table[str(i)]) + " is added to key[" + str(i) + "]")#Output to show the key is added
            return
        else:
            print(str(i) + " is occupied.")#Shows the key is occupied
    print(str(hash(value)) + " hash was not added to the table.")

def search_hash(key: str):#Get or Search the value
    if not key in hash_table:
        print("Invalid Input")
        return
    if hash_table.get(key) == "":#If the key is empty it has no value
        print(key + " has no value.")
    else:
        print("Key[" + key + "] has a hash value of [" + str(hash_table[key]) + "].")#Show if the value is not empty

def delete_hash(key: str):#Remove or Delete Value
    if not key in hash_table:
        print("Invalid Input")
        return
    if hash_table.get(key) == "":#If the key is empty it will not delete it
        print(key + " has no value.")
    else:
        print("Key [" + key + "] with value of [" + str(hash_table[key]) + "] has been deleted.")#If the key has a value it will remove or delete it
        hash_table[key] = ""

def show_table():
    empty = True
    for i in range(max_size):
        if hash_table[str(i)] != "":#Check if the key is occupied
            print("Key [" + str(i) + "] has a hash value of [" + str(hash_table[str(i)]) + "].\n")
            empty = False
    if empty:
        print("Table is empty.")

while True:#Main process
    answer: str = str(input("1: Input new value\n2: Search for a value\n3: Delete a value\n4: Show Table\n"))#Main process
    if answer == "1":#It will go to the add a value
        add_hash(str(input("Enter a value: ")))
    elif answer == "2":#It will go the Get or Search Value
        search_hash(str(input("Enter search key: ")))
    elif answer == "3":#It will go the Delete or Remove value
        delete_hash(str(input("Enter delete key: ")))
    elif answer =="4":
        show_table()
